#include "tset.h"
TSet::TSet(int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &set) : BitField(set.BitField)
{
	MaxPower = set.MaxPower;
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()
{
	return TBitField(MaxPower);
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
	return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &set) // присваивание
{
	if (this != &set)
	{
		BitField = set.BitField;
		MaxPower = set.MaxPower;
		return *this;
	}
	return *this;
}

int TSet::operator==(const TSet &set) const // сравнение
{
	return (MaxPower == set.MaxPower) & (BitField == set.BitField);
}

int TSet::operator!=(const TSet &set) const // сравнение
{
	return (MaxPower != set.MaxPower) | (BitField != set.BitField);
}


TSet TSet::operator+(const TSet &set) // объединение
{
	TSet temp = (BitField | set.BitField);
	return temp;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet result = *this;
	result.BitField.SetBit(Elem);
	return result;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet result = *this;
	result.BitField.ClrBit(Elem);
	return result;
}

TSet TSet::operator*(const TSet &set) // пересечение
{
	return (BitField & set.BitField);
}

TSet TSet::operator~(void) // дополнение
{
	return (~BitField);
}

// перегрузка ввода/вывода
//формат ввода: A1,A3,A4.
istream &operator >> (istream &istr, TSet &set) // ввод
{
	char ch;
	int i;
	while (true)
	{
		istr >> ch;
		istr >> i;
		set.InsElem(i);
		istr >> ch;
		if (ch == '.') break;
	}
	return istr;
}
//формат вывода: { A1 A2 ... An }
ostream& operator<<(ostream &ostr, const TSet &set) // вывод
{
	ostr << "{ ";
	for (int i = 0; i<set.GetMaxPower(); i++)
		if (set.IsMember(i))
			ostr << 'A' << i << ' ';
	ostr << "}";
	return ostr;
}